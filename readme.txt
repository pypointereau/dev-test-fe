Please supply a pull request of a forked repository containing the html and necessary files needed to build the webpage 
in the attached PSD, please ensure it is able to run as a static html page.

At Bauer we support all major modern browsers including backwards compatibility for Internet Explorer 8 and above, 
so please bear that in mind when you are preparing your files.

Generally we expect cross browser compatibility, although progressive enhancement is an 
acceptable methodology to avoid headaches in older browsers.

The supplied design will give us a good indication of the way you like to work, it�s your chance to stand out.