module.exports = (grunt) ->
	
	grunt.loadNpmTasks 'grunt-contrib-watch'
	grunt.loadNpmTasks 'grunt-contrib-sass'
	grunt.loadNpmTasks 'grunt-contrib-connect'
	grunt.loadNpmTasks 'grunt-open'
	grunt.loadNpmTasks 'grunt-autoprefixer'
	grunt.loadNpmTasks 'grunt-static-inline'
	grunt.loadNpmTasks 'grunt-contrib-imagemin'
	grunt.loadNpmTasks 'grunt-contrib-clean'
	grunt.loadNpmTasks 'grunt-contrib-copy'
	


	grunt.initConfig

		sass:
			options:
				style: 'compressed'
			final:
				files:
					'final/style.min.css': 'src/sass/style.scss'

		autoprefixer:
			final:
				expand: true
				flatten: true
				src: 'final/style.min.css'
				dest: 'final/'

		clean:
			files: 
				expand:	true
				cwd: 'final/'
				src: ['**/*']

		copy:
			main:
				files: [{
					expand: true
					cwd: 'src'
					dest: 'final'
					src: ['index.html','images/*']
				}]

		imagemin:
			final:
				files: [{
					expand: true
					cwd: 'final/images/'
					src: ['**/*.{png,jpg,gif}']
					dest: 'final/images/'
				}]

		staticinline:
			dist:
				options:
					basepath: 'final/'
				files:
					'final/index.html': 'final/index.html'

		connect:
			final:
				options:
					port: 9000
					hostname: 'localhost'
					base: './final'
		
		open:
			final:
				path: 'http://localhost:9000/'

		watch:
			options:
				livereload: true
			sass:
				files: 'src/sass/style.scss'
				tasks: ['sass', 'autoprefixer']
			html:
				files: ['final/index.html']


	
	# Compiling and Watching
	grunt.registerTask 'default', [
		'clean',
		'sass', 'autoprefixer',
		'copy',
		'imagemin',
		'connect', 'open', 'watch'
	]

	# Compiling, inlining and watching
	grunt.registerTask 'build', [
		'clean',
		'sass', 'autoprefixer',
		'copy',
		'staticinline',
		'imagemin',
		'connect', 'open', 'watch'
	]








